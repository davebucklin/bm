#
# Some functions and aliases for working with FLAC files
#
alias ren1="rename -e 's/.*? ([0-9][0-9] .*)/\$1/' *.flac"
alias compile="metaflac --remove-tag=ALBUMARTIST --set-tag=COMPILATION=1 *.flac"

list () {
  for f in *.flac; do
    track=$(gettrack "$f")
    title=$(gettitle "$f")
    artist=$(getartist "$f")
    album=$(getalbum "$f")
    printf "%s\t%s\t%s\t%s\n" "$track" "$artist" "$title" "$album" | column -t -s"	"
  done
}

flactomp3 () {
  #for f in *.flac; do
    track=$(gettrack "$1")
    title=$(gettitle "$1")
    artist=$(getartist "$1")
    album=$(getalbum "$1")
    dir=$(dirname "$1")
    flac --decode --stdout "$1" | lame --tt "$title" --ta "$artist" --tl "$album" --tn "$track" --preset standard - "$dir"/"$track"-"$title".mp3
  #done
}

gettrack () { track=$(metaflac --show-tag=TRACKNUMBER "$1") ; echo "${track#*=}"; }
rmtrack () { metaflac --remove-tag=TRACKNUMBER "$1"; }
settrack () { rmtrack "$1"; metaflac --set-tag=TRACKNUMBER="$2" "$1"; }

gettitle () { title=$(metaflac --show-tag=TITLE "$1") ; echo "${title#*=}"; }
rmtitle () { metaflac --remove-tag=TITLE "$1"; }
settitle () { rmtitle "$1"; metaflac --set-tag=TITLE="$2" "$1"; }

getalbum () { album=$(metaflac --show-tag=ALBUM "$1") ; echo "${album#*=}"; }
rmalbum () { metaflac --remove-tag=ALBUM "$1"; }
setalbum () { rmalbum "$1"; metaflac --set-tag=ALBUM="$2" "$1"; }

getartist () { artist=$(metaflac --show-tag=ARTIST "$1") ; echo "${artist#*=}"; }
rmartist () { metaflac --remove-tag=ARTIST "$1"; }
setartist () { rmartist "$1"; metaflac --set-tag=ARTIST="$2" "$1"; }

setallartist () { for f in *.flac; do settitle "$f" "$1"; done; }
setalltitle () { for f in *.flac; do settitle "$f" "$1"; done; }
setallalbum () { for f in *.flac; do setalbum "$f" "$1"; done; }

